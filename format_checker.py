from output import error_print
import re
ERROR = 1
CORRECT = 0

no_space_operands = []
operands = ['=', "==", "/", "&&"]

def check_line(l, line_num, filename):
    errors = 0 
    if '\t' in l:
        error_print(filename, line_num, "Tab found instead of 4 spaces", l)
        errors += 1
    if '{' in l and len(l.strip()) > 1:
        error_print(filename, line_num, "Opening bracket not on newline", l)
        errors += 1 
    return errors


def check_format(f):
    f.seek(0)
    errors_count = 0
    for num, line in enumerate(f.readlines()):
        errors_count += check_line(line, num, f.name)
    return errors_count
