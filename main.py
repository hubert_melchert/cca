import fnmatch
import os
import sys
import argparse
from switch_checker import check_switch
from debug_define import check_debug_define
from format_checker import check_format
import output


def print_loud(text):
    global args
    if not args.silent:
        print text


parser = argparse.ArgumentParser()
parser.add_argument("--silent", help="print only errors", action="store_true")
args = parser.parse_args()
if args.silent:
    output.set_indents(0)

matches = []
for root, dirnames, filenames in os.walk('.'):
    for filename in fnmatch.filter(filenames, '*.c'):
        matches.append(os.path.join(root, filename))
if len(matches) == 0:
    print "No *.c, *.h files found"
    sys.exit()

tests_list = [{"name": "Debug defines", "function": check_debug_define},
              {"name": "Switch",        "function": check_switch},
              {"name": "Code format",   "function": check_format}]

total_errors_count = 0
for filename in matches:
    try:
        print_loud("~~~~~~~\nTesting " + filename)
        with open(filename, 'r') as f:
            for test in tests_list:
                print_loud("    Testing " + test["name"])
                errors_count = test["function"](f)
                total_errors_count += errors_count
                print_loud("    " + test["name"] + " " +
                           ("OK" if errors_count == 0
                            else str(errors_count) + " ERROR(S)!\n"))
    except IOError:
        print "Error opening file", filename
print "_________________________"
print "Errors:", total_errors_count
