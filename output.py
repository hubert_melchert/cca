ERROR_INDENT = 8


def set_indents(error):
    global ERROR_INDENT
    ERROR_INDENT = error


def error_print(file_info, line_num, error_info, line_text):
    file_info = "(" + file_info + ")"
    print (ERROR_INDENT * " " + "error " + file_info +
           "[{}] ".format(str(line_num)) + error_info)
    for line in line_text.strip("\n").split("\n"):
        print (ERROR_INDENT + 4) * " " + ">" + line
