from output import error_print
ERROR = 1
CORRECT = 0


def check_define_in_line(l, num, filename):
    debug_names = ["debug", "test"]
    line = l.split()
    try:
        define_position = line.index("#define")
    except ValueError:
        return CORRECT

    if define_position >= 1 and line[define_position - 1] in ["//", "/*"]:
        return CORRECT

    if len(line) < define_position + 2:
        error_print(filename, num, "#define with no name", l)
        return ERROR

    define_name = line[define_position + 1]

    if not any([x in define_name.lower() for x in debug_names]):
        return CORRECT

    if len(line) == define_position + 2:
        # case with empty definition for later use in ifdef
        error_print(filename, num, "Debug/test #define is empty"
                    "(considered set)", l)
        return ERROR

    if line[define_position + 2].strip() not in ["0", "False", "false"]:
        error_print(filename, num, "Debug/test #define is set", l)
        return ERROR
    return CORRECT


def check_debug_define(f):
    f.seek(0)
    errors_count = 0
    for num, line in enumerate(f.readlines()):
        if "#define" in line:
            errors_count += check_define_in_line(line, num, f.name)
    return errors_count
