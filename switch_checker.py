from output import error_print
ERROR = 1
CORRECT = 0


def check_switch(f):
    f.seek(0)
    errors_count = 0
    wait_for_break = False
    previous_line_was_case = True
    previous_case_line_text = ""
    for num, line in enumerate(f.readlines()):
        if "case" in line:
            if wait_for_break and not previous_line_was_case:
                error_print(f.name, num, "Missing break in switch",
                            previous_case_line_text)
                errors_count += 1
                wait_for_break = False
                previous_line_was_case = False
            else:
                wait_for_break = True
                previous_case_line_text = line
                previous_line_was_case = True
            continue
        previous_line_was_case = False
        if "break" in line and wait_for_break:
            wait_for_break = False
            previous_line_was_case = False
        elif "default" in line and wait_for_break:
            error_print(f.name, num, "Missing break in switch",
                        previous_case_line_text)
    return errors_count
